<?include "header.php";?>
<?include "include/public_header.php";?>
    <section class="wrapper main__screen">
        <div class="container">
            <div class="row">
                <?include "include/sidebar.php";?>
                <div class="maincontainer col-lg-10 col-12">
                    <div class="heading__block d-flex flex-wrap justify-content-between align-items-center">
                        <div class="heading__text">
                            Отправить запрос на расчет
                        </div>
                    </div>
                    <div class="row">
                        <form target="_self" action="test.php" class="ajax form centeredform col-lg-11 col-xl-9 col-12" data-successfunc="CreateFailed">
                            <div class="formfield row">
                                <div class="col-md-6 col-12 label">ИНН клиента*</div>
                                <div class="col-md-6 col-12 ">
                                    <div class="input">
                                        <input class="short onlynum" data-required="text"/>
                                        <a class="button blue mini" data-popup="popup1">Предыдущие сделки</a>
                                    </div>
                                </div>
                            </div>
                            <div class="formfield row">
                                <div class="col-md-6 col-12 label">Организационно-правовая форма клиента</div>
                                <div class="col-md-6 col-12">
                                    <select class="select2">
                                        <option>Общество с ограниченной ответственностью</option>
                                        <option>Индивидуальный предприниматель</option>
                                    </select>
                                </div>
                            </div>
                            <div class="formfield row">
                                <div class="col-md-6 col-12 label">Наименование клиента (организации)*</div>
                                <div class="col-md-6 col-12 ">
                                    <input value="Машины и трактора"/>
                                </div>
                            </div>
                            <div class="formfield row">
                                <div class="col-md-6 col-12 label">Контактное лицо клиента*</div>
                                <div class="col-md-6 col-12 ">
                                    <input value="Дементьева Елена Владимировна"/>
                                </div>
                            </div>
                            <div class="formfield row">
                                <div class="col-md-6 col-12 label">Телефон для связи с клиентом</div>
                                <div class="col-md-6 col-12 ">
                                    <input name="phone" value="+7 925 456-63-90"/>
                                </div>
                            </div>
                            <div class="formfield row">
                                <div class="col-md-6 col-12 label">Площадка/поставщик</div>
                                <div class="col-md-6 col-12 ">
                                    <input value="ГК «Восток». Восток-ДВ/Енисейская застава"/>
                                </div>
                            </div>
                            <div class="formfield row">
                                <div class="col-md-6 col-12 label">Марка транспортного средства</div>
                                <div class="col-md-6 col-12 ">
                                    <div class="input short">
                                        <select class="select2">
                                            <option>Mercedes-Benz</option>
                                            <option>Audi</option>
                                            <option>Man</option>
                                            <option>Ваз</option>
                                            <option>Газ</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="formfield row">
                                <div class="col-md-6 col-12 label">Модель транспортного средства*</div>
                                <div class="col-md-6 col-12">
                                    <div class="input short">
                                        <select class="select2">
                                            <option>E 63</option>
                                            <option>GL 65</option>
                                            <option>GL 55</option>
                                            <option>GLE 65</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="formfield row">
                                <div class="col-md-6 col-12 label">Стоимость</div>
                                <div class="col-md-6 col-12 ">
                                    <input value="1 250 000 р."/>
                                </div>
                            </div>
                            <div class="formfield row">
                                <div class="col-md-6 col-12 label">Комментарий</div>
                                <div class="col-md-6 col-12 ">
                                    <input value="Срочно в работу!"/>
                                </div>
                            </div>
                            <div class="formfield row">
                                <div class="col-12">
                                    <button class="button blue">Отправить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?include "footer.php";?>