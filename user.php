<?include "header.php";?>
<?include "include/public_header.php";?>
<section class="wrapper main__screen">
    <div class="container">
        <div class="row align-items-start">
            <?include "include/sidebar.php";?>
            <div class="maincontainer col-lg-10 col-12">
                <div class="heading__block d-flex justify-content-between align-items-center">
                    <div class="heading__text">
                        Личные данные
                    </div>
                    <div class="heading__actions">
                        <a href="#" class="user__goedit">
                            <span class="icon icon-icons-18"></span>
                            <span class="value">Редактировать</span>
                        </a>
                    </div>
                </div>
                <div class="user__block">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="user__maininfo d-flex">
                                <div class="photo" style="background-image: url(images/user-big.png);"></div>
                                <div class="content">
                                    <div class="name">Павликовская Екатерина Сергеевна</div>
                                    <div class="role">
                                        руководитель отдела по финансовым делам регионального отделения
                                    </div>
                                    <div class="city">г. Новосибирск</div>
                                    <div class="user__contacts">
                                        <div class="user__heading">
                                            Контакты
                                        </div>
                                        <div class="user__contacts__item">
                                            <div class="icon">
                                                <img src="images/icons-phone.svg"/>
                                            </div>
                                            <div class="value">
                                                <a href="tel:+7 (925) 456-63-90">+7 (925) 456-63-90</a>
                                                <a href="tel:+7 (925) 434-63-32">+7 (925) 434-63-32</a>
                                            </div>
                                        </div>
                                        <div class="user__contacts__item">
                                            <div class="icon">
                                                <img src="images/icons-mail.svg"/>
                                            </div>
                                            <div class="value">
                                                <a href="mailto:pavlik@mail.ru">pavlik@mail.ru</a>
                                            </div>
                                        </div>
                                        <div class="user__contacts__item">
                                            <div class="icon">
                                                <img src="images/icons-telegram.svg"/>
                                            </div>
                                            <div class="value">
                                                <a>@element_pavlik</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="user__password">
                                        <div class="user__heading">
                                            Смена пароля
                                        </div>
                                        <div class="user__password__form">
                                            <div class="formfield">
                                                <div class="label">Пароль</div>
                                                <input type="password"/>
                                            </div>
                                            <div class="formfield">
                                                <div class="label">Новый пароль</div>
                                                <input type="password"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="user__metriks">
                                <a href="summary.detail.director.php" class="user__metrik">
                                    <div class="count">4</div>
                                    <div class="value">Активные заявки</div>
                                </a>
                                <a href="summary.detail.director.php" class="user__metrik">
                                    <div class="count">0</div>
                                    <div class="value">Оплаченые заявки</div>
                                </a>
                                <a href="summary.detail.director.php" class="user__metrik">
                                    <div class="count">1</div>
                                    <div class="value">Отказ</div>
                                </a>
                                <a href="summary.detail.director.php" class="user__metrik">
                                    <div class="count">1</div>
                                    <div class="value">Акт ПП</div>
                                </a>
                            </div>
                        </div>
                        <div class="col-12 d-flex justify-content-end">
                            <a class="button blue">Сохранить</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?include "footer.php";?>
