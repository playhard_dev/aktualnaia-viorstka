<?include "header.php";?>
<?include "include/public_header.php";?>
    <section class="wrapper main__screen">
        <div class="container">
            <div class="row">
                <?include "include/sidebar.php";?>
                <div class="maincontainer col-lg-10 col-12">
                    <div class="heading__block d-flex flex-wrap justify-content-between align-items-center">
                        <div class="heading__text">
                            Создать расчет
                        </div>
                    </div>
                    <div class="row justify-content-between">
                        <form id="form1" target="_self" action="test.php" class="ajax form centeredform col-lg-11 col-xl-9 col-12" data-successfunc="CreateSuccess">
                            <div class="form__step active">
                                <div class="form__step__heading">1. Общие данные по клиенту</div>
                                <div class="formfield row">
                                    <div class="switcher">
                                        <label>
                                            <input checked type="radio" name="type0"/>
                                            <span>Юр. лицо</span>
                                        </label>
                                        <label>
                                            <input type="radio" name="type0"/>
                                            <span>Физическое лицо</span>
                                        </label>
                                        <label>
                                            <input type="radio" name="type0"/>
                                            <span>ИП</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="formfield row">
                                    <div class="col-md-6 col-12 label">ИНН клиента*</div>
                                    <div class="col-md-6 col-12 ">
                                        <div class="input">
                                            <input class="short onlynum" data-required="text"/>
                                            <a class="button blue mini" data-popup="popup1">Предыдущие сделки</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="formfield row">
                                    <div class="col-md-6 col-12 label">Организационно-правовая форма клиента</div>
                                    <div class="col-md-6 col-12">
                                        <div class="input">
                                            <select class="select2">
                                                <option>Общество с ограниченной ответственностью</option>
                                                <option>Индивидуальный предприниматель</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="formfield row">
                                    <div class="col-md-6 col-12 label">Наименование клиента (организации)*</div>
                                    <div class="col-md-6 col-12 ">
                                        <div class="input">
                                            <input value="Машины и трактора" data-required="text"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="formfield row">
                                    <div class="col-md-6 col-12 label">Срок деятельности организации*</div>
                                    <div class="col-md-6 col-12 ">
                                        <div class="input">
                                            <div class="input__wrapper">
                                                <input class="width-dynamic" value="" data-required="text"/>
                                                <div class="input__caption" data-value="мес."></div>
                                            </div>
                                            <div class="input__slider" data-min="1" data-max="350" data-step="1"></div>
                                            <div class="input__reference">
                                                <span>
                                                    При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце, что позволяет сделать текст более привлекательным и живым для визуально-слухового восприятия.
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="formfield row">
                                    <div class="col-md-6 col-12 label">Сколько единиц автотранспорта, который клиент использует в бизнесе, находится в его собственности?</div>
                                    <div class="col-md-6 col-12 ">
                                        <div class="input">
                                            <div class="input__wrapper short">
                                                <input class="onlynum width-dynamic" value="0"/>
                                                <div class="input__caption" data-value="ед."></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="formfield row">
                                    <div class="col-md-6 col-12 label">Ежемесячная выручка клиента</div>
                                    <div class="col-md-6 col-12 ">
                                        <div class="input">
                                            <div class="input__wrapper short">
                                                <input class="onlynum width-dynamic" value="0"/>
                                                <div class="input__caption" data-value="р."></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="formbtns row">
                                    <div class="col-12">
                                        <a class="button blue mini next">Далее</a>
                                    </div>
                                </div>
                            </div>
                            <div class="form__step">
                                <div class="form__step__heading">2. Информация о приобретаемом транспортном средстве</div>
                                <div class="formfield row">
                                    <div class="col-md-6 col-12 label">Марка транспортного средства</div>
                                    <div class="col-md-6 col-12 ">
                                        <div class="input short">
                                            <select class="select2">
                                                <option>Mercedes-Benz</option>
                                                <option>Audi</option>
                                                <option>Man</option>
                                                <option>Ваз</option>
                                                <option>Газ</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="formfield row">
                                    <div class="col-md-6 col-12 label">Модель транспортного средства*</div>
                                    <div class="col-md-6 col-12">
                                        <div class="input short">
                                            <select class="select2">
                                                <option>E 63</option>
                                                <option>GL 65</option>
                                                <option>GL 55</option>
                                                <option>GLE 65</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="formfield row">
                                    <div class="col-md-6 col-12 label">Продукт/Акция*</div>
                                    <div class="col-md-6 col-12 ">
                                        <div class="input short">
                                            <select class="select2">
                                                <option>Продукт 1</option>
                                                <option>Продукт 2</option>
                                                <option>Акция 1</option>
                                                <option>Акция 2</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="formfield row">
                                    <div class="col-md-6 col-12 label">Стоимость, руб.*</div>
                                    <div class="col-md-6 col-12 ">
                                        <div class="input">
                                            <div class="input__wrapper short">
                                                <input class="width-dynamic" value="" data-required="text"/>
                                                <div class="input__caption" data-value="р."></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="formfield row">
                                    <div class="col-md-6 col-12 label">Площадка/Поставщик</div>
                                    <div class="col-md-6 col-12 ">
                                        <div class="input">
                                            <select class="select2">
                                                <option>ГК «Восток». Восток-ДВ/Енисейская застава</option>
                                                <option>ГК «Восток». Восток-ДВ/Енисейская застава</option>
                                                <option>ГК «Восток». Восток-ДВ/Енисейская застава</option>
                                                <option>ГК «Восток». Восток-ДВ/Енисейская застава</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="formbtns row">
                                    <div class="col-12">
                                        <a class="button white mini prev">Назад</a>
                                        <a class="button blue mini next">Далее</a>
                                    </div>
                                </div>
                            </div>
                            <div class="form__step">
                                <div class="form__step__heading">3. Условия лизинга</div>
                                <div class="from__attention d-flex align-items-center">
                                    <div class="icon" style="margin-top: -24px;">
                                        <img src="images/img-enter.png"/>
                                    </div>
                                    <div class="text">
                                        Для выбранного продукта ( акции) сумма аванса должна быть не менее 15%, а срок лизинга не более 36 мес.
                                    </div>
                                </div>
                                <div class="formfield row">
                                    <div class="col-md-6 col-12 label">Выберите размер аванса, %</div>
                                    <div class="col-md-6 col-12 ">
                                        <div class="input middle">
                                            <div class="input__wrapper">
                                                <input class="width-dynamic" value="" data-required="text"/>
                                                <div class="input__caption" data-value="%"></div>
                                            </div>
                                            <div class="input__slider" data-min="1" data-max="100" data-step="1"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="formfield row">
                                    <div class="col-md-6 col-12 label">Срок лизинга, мес.*</div>
                                    <div class="col-md-6 col-12 ">
                                        <div class="input middle">
                                            <div class="input__wrapper">
                                                <input class="width-dynamic" value="" data-required="text"/>
                                                <div class="input__caption" data-value="мес."></div>
                                            </div>
                                            <div class="input__slider" data-min="1" data-max="36" data-step="1"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="formbtns row">
                                    <div class="col-12">
                                        <a class="button white mini prev">Назад</a>
                                        <a class="button blue mini next">Далее</a>
                                    </div>
                                </div>
                            </div>
                            <div class="form__step">
                                <div class="form__step__heading">4. График платежей</div>
                                <div class="table__wrapper">
                                    <table id="customtable" class="customtable table">
                                        <thead class="table__row table__head">
                                            <tr>
                                                <td class="sort table__cell"></td>
                                                <td class="sort table__cell" data-sort="name">Платежи</td>
                                                <td class="sort table__cell" data-sort="middle">Средний ежемес. платеж, р.</td>
                                                <td class="sort table__cell" data-sort="income">Удорожание, %</td>
                                                <td class="sort table__cell" data-sort="sum">Итого, р.</td>
                                                <td class="sort table__cell"></td>
                                            </tr>
                                        </thead>
                                        <tbody class="list">
                                            <tr class="table__row">
                                                <td class="table__cell">
                                                    <label class="table__checker">
                                                        <input checked type="radio" name="type1"/>
                                                        <span class="table__checker__fake"></span>
                                                    </label>
                                                </td>
                                                <td class="name table__cell">Платежи с дегрессией</td>
                                                <td class="middle table__cell">54 654,23</td>
                                                <td class="income table__cell">9,31</td>
                                                <td class="sum table__cell">3 883 541</td>
                                                <td class="sum table__cell"><a data-popup="popup3" class="view__btn icon-icons-10"></a></td>
                                            </tr>
                                            <tr class="table__row">
                                                <td class="table__cell">
                                                    <label class="table__checker">
                                                        <input type="radio" name="type1"/>
                                                        <span class="table__checker__fake"></span>
                                                    </label>
                                                </td>
                                                <td class="name table__cell">Равные платежи</td>
                                                <td class="middle table__cell">123 568 897,15</td>
                                                <td class="income table__cell">17,62</td>
                                                <td class="sum table__cell">4 858 265</td>
                                                <td class="sum table__cell"><a data-popup="popup3" class="view__btn icon-icons-10"></a></td>
                                            </tr>
                                            <tr class="table__row">
                                                <td class="table__cell">
                                                    <label class="table__checker">
                                                        <input checked type="radio" name="type2"/>
                                                        <span class="table__checker__fake"></span>
                                                    </label>
                                                </td>
                                                <td class="name table__cell">Плавно убывающие</td>
                                                <td class="middle table__cell">26 364,65</td>
                                                <td class="income table__cell">11,63</td>
                                                <td class="sum table__cell">4 045 654</td>
                                                <td class="sum table__cell"><a data-popup="popup3" class="view__btn icon-icons-10"></a></td>
                                            </tr>
                                            <tr class="table__row">
                                                <td class="table__cell">
                                                    <label class="table__checker">
                                                        <input type="radio" name="type2"/>
                                                        <span class="table__checker__fake"></span>
                                                    </label>
                                                </td>
                                                <td class="name table__cell">Резко убывающие</td>
                                                <td class="middle table__cell">101 564,45</td>
                                                <td class="income table__cell">13,97</td>
                                                <td class="sum table__cell">4 658 356</td>
                                                <td class="sum table__cell"><a data-popup="popup3" class="view__btn icon-icons-10"></a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form__result d-flex">
                                    <div class="icon">
                                        <img src="images/man_big.png"/>
                                    </div>
                                    <div class="text">
                                        <p>
                                            Выкупная стоимость: <b>1 500,00 р.</b>
                                        </p>
                                        <p>
                                            Комиссия за оформление: <b>60 000,00 р.</b>
                                        </p>
                                        <p>
                                            Оплата КАСКО и ОСАГО обязательна на весь срок лизинга.
                                        </p>
                                        <p>
                                            Чтобы узнать, возможно ли этот автомобиль приобрести в лизинг дешевле, закажите обратный звонок или напишите в онлайн-консультант.
                                        </p>
                                    </div>
                                </div>
                                <div class="formbtns row">
                                    <div class="col-12">
                                        <a class="button white mini prev">Назад</a>
                                        <button class="button blue mini">Отправить</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="formsteps col-2 p-0" data-formid="form1">
                            <div class="formstep active">
                                <div class="label">
                                    шаг 1
                                </div>
                                <div class="text">
                                    Общие данные
                                    по клиенту
                                </div>
                            </div>
                            <div class="formstep">
                                <div class="label">
                                    шаг 2
                                </div>
                                <div class="text">
                                    Информация
                                    о транспортном
                                    средстве
                                </div>
                            </div>
                            <div class="formstep">
                                <div class="label">
                                    шаг 3
                                </div>
                                <div class="text">
                                    Условия лизинга
                                </div>
                            </div>
                            <div class="formstep">
                                <div class="label">
                                    шаг 4
                                </div>
                                <div class="text">
                                    Общие данные
                                    по клиенту
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?include "footer.php";?>