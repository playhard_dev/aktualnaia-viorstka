<div class="mainsidebar col-2 hidden-md-down p-0">
    <div class="mainmenu">
        <a class="d-flex">
            <span class="icon-icons-20"></span>
            <span class="label">Сделки</span>
        </a>
        <a class="d-flex disabled">
            <span class="icon-icons-19"></span>
            <span class="label">Создать расчет</span>
        </a>
        <a class="d-flex disabled">
            <span class="icon-icons-21"></span>
            <span class="label">Финансовые показатели</span>
        </a>
        <a class="d-flex">
            <span class="icon-icons-25"></span>
            <span class="label">Сотрудники</span>
        </a>
    </div>
    <div class="mainsidebar__contacts col-12 d-flex flex-wrap">
        <div>
            <span class="icon-icons-17"></span>
        </div>
        <div>
            <div class="label">Техническая поддержка</div>
            <div class="value"><a href="tel:8 (800) 656-65-65">8 (800) 656-65-65</a></div>
        </div>
    </div>
    <div class="manager col-12">
        <div class="manager__heading">Ваш менеджер:</div>
        <div class="manager__photo" style="background-image: url(images/man_mini.png);"></div>
        <div class="manager__name">Голованов К.Н.</div>
        <div class="manager__role">
            старший менеджер лизингового отдела
        </div>
        <div class="manager__phones">
            <a>+7 (925) 456-63-90</a>
            <a>+7 (925) 434-63-32</a>
        </div>
        <div class="manager__mail">
            <a>fedor3000@element-lizing.ru</a>
        </div>
        <div class="manager__telegram">
            <a>@element_fedor3000</a>
        </div>
    </div>
</div>