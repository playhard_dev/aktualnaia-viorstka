<header class="wrapper header__block">
    <div class="container">
        <div class="row justify-content-between align-items-start">
            <div class="header__logo col-lg-2 col-md-3 col-4">
                <a>
                    <img src="images/logo.svg"/>
                </a>
            </div>
            <div class="header__search col-4 hidden-sm-down">
                <form>
                    <input type="text" placeholder="Поиск"/>
                    <button><span class="icon-icons-40"></span></button>
                </form>
            </div>
            <div class="col-lg-4 col-2 hidden-md-down"></div>
            <div class="header__userinfo col-lg-2 col-3 hidden-md-down d-flex align-items-center justify-content-between">
                <div class="textinfo">
                    <div class="name"><a href="user.php">Раскольников Ф.М.</a></div>
                    <div class="role">Руководитель</div>
                </div>
                <div class="photo" style="background-image: url(images/man_mini.png);">
                    <a href="user.php"></a>
                </div>
                <div class="additional__info">
                    <div class="name"><a href="user.php">Раскольников Ф.М.</a></div>
                    <div class="fullrole">
                        старший руководитель отдела
                        по финансовым делам регионального отделения
                        г. Новосибирск
                    </div>
                    <div class="exit">
                        <a href="login.php"><span class="icon-icons-28"></span>Выйти</a>
                    </div>
                </div>
            </div>
            <div class="mobile__head__right hidden-lg-up">
                <div class="mobile__head__btn">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </div>
</header>