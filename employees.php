<?include "header.php";?>
<?include "include/public_header.php";?>
<section class="wrapper main__screen">
    <div class="container">
        <div class="row">
            <?include "include/sidebar.php";?>
            <div class="maincontainer col-lg-10 col-12">
                <div class="heading__block d-flex justify-content-between">
                    <div class="heading__text">
                        Сотрудники
                    </div>
                </div>
                <div class="table__wrapper">
                    <table id="customtable" class="customtable table">
                        <thead class="table__row table__head">
                        <tr>
                            <td class="sort table__cell" data-sort="name">Ф.И.О</td>
                            <td class="sort table__cell" data-sort="mail">Логин</td>
                            <td class="sort table__cell" data-sort="role">Роль</td>
                            <td class="sort table__cell center" data-sort="access">Доступ</td>
                            <td class="table__cell"></td>
                            <td class="table__cell"></td>
                        </tr>
                        </thead>
                        <tbody class="list">
                        <tr class="table__row lock">
                            <td class="name table__cell lock-depend">
                                <div class="table__man__item">
                                    <div class="photo online" style="background-image: url(images/man1.png);"></div>
                                    <div class="name">
                                        Яковлена Анна Евгеньевна
                                    </div>
                                </div>
                            </td>
                            <td class="mail table__cell lock-depend">smitham.kendall@gmail.com</td>
                            <td class="role table__cell lock-depend">Менеджер</td>
                            <td class="access table__cell center row__footer">
                                <a class="lock__btn lock"></a>
                            </td>
                            <td class="table__cell row__footer">
                                <a class="del__btn icon-icons-23"></a>
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="name table__cell lock-depend">
                                <div class="table__man__item">
                                    <div class="photo offline" style="background-image: url(images/man2.png);"></div>
                                    <div class="name">
                                        Петровский Владимир Ильич
                                    </div>
                                </div>
                            </td>
                            <td class="mail table__cell lock-depend">vernie.stiedemann@quinten.io</td>
                            <td class="role table__cell lock-depend">Менеджер</td>
                            <td class="access table__cell center row__footer">
                                <a class="lock__btn unlock"></a>
                            </td>
                            <td class="table__cell row__footer">
                                <a class="del__btn icon-icons-23"></a>
                            </td>
                        </tr>
                        <tr class="table__row lock">
                            <td class="name table__cell lock-depend">
                                <div class="table__man__item">
                                    <div class="photo online" style="background-image: url(images/man3.png);"></div>
                                    <div class="name">
                                        Григорьев Петр Алексеевич
                                    </div>
                                </div>
                            </td>
                            <td class="mail table__cell lock-depend">hansen_juvenal@hotmail.com</td>
                            <td class="role table__cell lock-depend">Менеджер</td>
                            <td class="access table__cell center row__footer">
                                <a class="lock__btn unlock"></a>
                            </td>
                            <td class="table__cell row__footer">
                                <a class="del__btn icon-icons-23"></a>
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="name table__cell lock-depend">
                                <div class="table__man__item">
                                    <div class="photo offline" style="background-image: url(images/man4.png);"></div>
                                    <div class="name">
                                        Дроздова Евгения Алексеевна
                                    </div>
                                </div>
                            </td>
                            <td class="mail table__cell lock-depend">collin_torp@yahoo.com</td>
                            <td class="role table__cell lock-depend">Менеджер</td>
                            <td class="access table__cell center row__footer">
                                <a class="lock__btn unlock"></a>
                            </td>
                            <td class="table__cell row__footer">
                                <a class="del__btn icon-icons-23"></a>
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="name table__cell lock-depend">
                                <div class="table__man__item">
                                    <div class="photo offline" style="background-image: url(images/man5.png);"></div>
                                    <div class="name">
                                        Архангельский Максим Иванович
                                    </div>
                                </div>
                            </td>
                            <td class="mail table__cell lock-depend">emard_alessandro@blake.biz</td>
                            <td class="role table__cell lock-depend">Менеджер</td>
                            <td class="access table__cell center row__footer">
                                <a class="lock__btn unlock"></a>
                            </td>
                            <td class="table__cell row__footer">
                                <a class="del__btn icon-icons-23"></a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<?include "footer.php";?>