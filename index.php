<?include "header.php";?>
<?include "include/public_header.php";?>
<section class="wrapper main__screen">
    <div class="container">
        <div class="row">
            <?include "include/sidebar.php";?>
            <div class="maincontainer col-lg-10 col-12">
                <div class="heading__block d-flex justify-content-between align-items-center">
                    <div class="heading__text">
                        Финансовые показатели
                    </div>
                    <div class="heading__actions">
                        <div class="button__group">
                            <a class="button micro blue">Cегодня</a>
                            <a class="button micro blue">5 дней</a>
                            <a class="button micro blue active">1 месяц</a>
                            <span class="button__group__label">Период</span>
                        </div>
                    </div>
                </div>
                <div>
                    <a class="button white colorblue">
                        <span class="icon-icons-26"></span>
                        <span class="value">Скачать PDF</span>
                    </a>
                    <a class="button white colorblue">
                        <span class="icon-icons-16"></span>
                        <span class="value">Отправить на почту</span>
                    </a>
                </div>
                <div class="slideup__block">
                    <div class="slideup__item open">
                        <div class="slideup__header d-flex justify-content-between">
                            <div class="slideup__heading d-flex">
                                <div class="slideup__btn">
                                    <a><span></span></a>
                                </div>
                                <div class="slideup__heading__text">
                                    Заявок находится на расчете
                                </div>
                                <div class="count__caption">
                                    24
                                </div>
                            </div>
                            <div class="slideup__actions d-flex">
                                <select class="select2">
                                    <option>Январь</option>
                                    <option>Февраль</option>
                                    <option>Март</option>
                                    <option>Апрель</option>
                                    <option>Май</option>
                                    <option>Июнь</option>
                                    <option>Июль</option>
                                    <option>Август</option>
                                    <option>Сентябрь</option>
                                    <option>Октябрь</option>
                                    <option>Ноябрь</option>
                                    <option selected>Декабрь</option>
                                </select>
                                <select class="select2">
                                    <option>2015</option>
                                    <option>2016</option>
                                    <option>2017</option>
                                    <option selected>2018</option>
                                    <option>2019</option>
                                </select>
                                <div class="slideup__price">
                                    10 000 000 р.
                                </div>
                            </div>
                        </div>
                        <div class="slideup__body">
                            <div class="slideup__body__inner row">
                                <div class="col-lg-4 col-12">
                                    <div class="graph1"></div>
                                </div>
                                <script>
                                    $(document).ready(function(){
                                        var chart = bb.generate({
                                            data: {
                                                columns: [
                                                    ["data1", 55013],
                                                    ["data2", 63019],
                                                    ["data3", 51465],
                                                    ["data4", 90719],
                                                    ["data5", 52941]
                                                ],
                                                type: "donut",
                                                colors: {
                                                    data1: "rgba(255,110,79,1)",
                                                    data2: "rgba(255,200,0,1)",
                                                    data3: "rgba(255,164,190,1)",
                                                    data4: "rgba(0,119,255,1)",
                                                    data5: "rgba(255,201,111,1)"
                                                },
                                                names: {
                                                    data1: "Северсталь",
                                                    data2: "МТС медиа",
                                                    data3: "Газмедиагрупп",
                                                    data4: "Иванченко Л.С.",
                                                    data5: "Лизинг LTD."
                                                }
                                            },
                                            transition: {
                                                duration: 100
                                            },
                                            legend: {
                                                show: false
                                            },
                                            labels: false,
                                            bindto: ".graph1"
                                        });
                                    });
                                </script>
                                <div class="col-lg-8 col-12">
                                    <div class="table__wrapper">
                                        <table id="customtable" class="customtable table mini">
                                            <tbody class="list">
                                                <tr class="table__row">
                                                    <td class="table__cell">
                                                        <span class="table__indicator red"></span>
                                                    </td>
                                                    <td class="name table__cell">Северсталь</td>
                                                    <td class="mail table__cell">12 дек. 18</td>
                                                    <td class="role table__cell">Денисов Н.Б.</td>
                                                    <td class="access table__cell">55 013 р.</td>
                                                </tr>
                                                <tr class="table__row">
                                                    <td class="table__cell">
                                                        <span class="table__indicator yellow"></span>
                                                    </td>
                                                    <td class="name table__cell">МТС медиа</td>
                                                    <td class="mail table__cell">9 дек. 18</td>
                                                    <td class="role table__cell">Самышкина Т.В.</td>
                                                    <td class="access table__cell">63 019 р.</td>
                                                </tr>
                                                <tr class="table__row">
                                                    <td class="table__cell">
                                                        <span class="table__indicator pink"></span>
                                                    </td>
                                                    <td class="name table__cell">Газмедиагрупп</td>
                                                    <td class="mail table__cell">8 дек. 18</td>
                                                    <td class="role table__cell">Гераськин И.В.</td>
                                                    <td class="access table__cell">51 465  р.</td>
                                                </tr>
                                                <tr class="table__row">
                                                    <td class="table__cell">
                                                        <span class="table__indicator blue"></span>
                                                    </td>
                                                    <td class="name table__cell">Иванченко Л.С.</td>
                                                    <td class="mail table__cell">3 дек. 18</td>
                                                    <td class="role table__cell">Егоров Д.В.</td>
                                                    <td class="access table__cell">90 719 р.</td>
                                                </tr>
                                                <tr class="table__row">
                                                    <td class="table__cell">
                                                        <span class="table__indicator orange"></span>
                                                    </td>
                                                    <td class="name table__cell">Лизинг LTD.</td>
                                                    <td class="mail table__cell">3 дек. 18</td>
                                                    <td class="role table__cell">Павликовская Е.Н.</td>
                                                    <td class="access table__cell">52 941 р.</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center">
                                <a class="button mini white">Все заявки</a>
                            </div>
                        </div>
                    </div>
                    <div class="slideup__item open">
                        <div class="slideup__header d-flex justify-content-between">
                            <div class="slideup__heading d-flex">
                                <div class="slideup__btn">
                                    <a><span></span></a>
                                </div>
                                <div class="slideup__heading__text">
                                    Создано счетов
                                </div>
                                <div class="count__caption">
                                    1000
                                </div>
                            </div>
                            <div class="slideup__actions d-flex">
                                <select class="select2">
                                    <option>Январь</option>
                                    <option>Февраль</option>
                                    <option>Март</option>
                                    <option>Апрель</option>
                                    <option>Май</option>
                                    <option>Июнь</option>
                                    <option>Июль</option>
                                    <option>Август</option>
                                    <option>Сентябрь</option>
                                    <option>Октябрь</option>
                                    <option>Ноябрь</option>
                                    <option selected>Декабрь</option>
                                </select>
                                <select class="select2">
                                    <option>2015</option>
                                    <option>2016</option>
                                    <option>2017</option>
                                    <option selected>2018</option>
                                    <option>2019</option>
                                </select>
                                <div class="slideup__price">
                                    120 000 000 р.
                                </div>
                            </div>
                        </div>
                        <div class="slideup__body">
                            <div class="slideup__body__inner row">
                                <div class="col-12">
                                    <div class="graph2"></div>
                                </div>
                                <script>
                                    $(document).ready(function(){
                                        var chart = bb.generate({
                                            data: {
                                                columns: [
                                                    ["Прибыль", 250000, 390000, 15000, 50000, 450000, 741000, 260000, 562000]
                                                ],
                                                type: "area",
                                                colors: {
                                                    data1: "#89C0FF"
                                                }
                                            },
                                            axis: {
                                                x: {
                                                    type: "category",
                                                    categories: [
                                                        "12 дек",
                                                        "13 дек",
                                                        "14 дек",
                                                        "15 дек",
                                                        "16 дек",
                                                        "17 дек",
                                                        "18 дек",
                                                        "19 дек"
                                                    ]
                                                }
                                            },
                                            legend: {
                                                show: false
                                            },
                                            zoom: {
                                                enabled: true
                                            },
                                            labels: false,
                                            bindto: ".graph2"
                                        });
                                    });
                                </script>
                            </div>
                            <div class="d-flex justify-content-center">
                                <a class="button mini white">Все расчёты</a>
                            </div>
                        </div>
                    </div>
                    <div class="slideup__item open">
                        <div class="slideup__header d-flex justify-content-between">
                            <div class="slideup__heading d-flex">
                                <div class="slideup__btn">
                                    <a><span></span></a>
                                </div>
                                <div class="slideup__heading__text">
                                    Оплаченные проекты
                                </div>
                                <div class="count__caption">
                                    5
                                </div>
                            </div>
                            <div class="slideup__actions d-flex">
                                <select class="select2">
                                    <option>Январь</option>
                                    <option>Февраль</option>
                                    <option>Март</option>
                                    <option>Апрель</option>
                                    <option>Май</option>
                                    <option>Июнь</option>
                                    <option>Июль</option>
                                    <option>Август</option>
                                    <option>Сентябрь</option>
                                    <option>Октябрь</option>
                                    <option>Ноябрь</option>
                                    <option selected>Декабрь</option>
                                </select>
                                <select class="select2">
                                    <option>2015</option>
                                    <option>2016</option>
                                    <option>2017</option>
                                    <option selected>2018</option>
                                    <option>2019</option>
                                </select>
                                <div class="slideup__price">
                                    120 000 000 р.
                                </div>
                            </div>
                        </div>
                        <div class="slideup__body">
                            <div class="slideup__body__inner row">
                                <div class="col-12">
                                    <div class="table__wrapper">
                                        <table id="customtable2" class="customtable table">
                                            <thead class="table__row table__head">
                                            <tr>
                                                <td class="sort table__cell" data-sort="name">Клиент</td>
                                                <td class="sort table__cell" data-sort="date">Дата оплаты</td>
                                                <td class="sort table__cell" data-sort="create">Кем создано</td>
                                                <td class="sort table__cell" data-sort="sum">Сумма, р.</td>
                                            </tr>
                                            </thead>
                                            <tbody class="list">
                                                <tr class="table__row lock">
                                                    <td class="name table__cell">1. Предприниматель</td>
                                                    <td class="date table__cell">9 дек. 2018</td>
                                                    <td class="create table__cell">Денисов Н.Б.</td>
                                                    <td class="sum table__cell">500 000</td>
                                                </tr>
                                                <tr class="table__row lock">
                                                    <td class="name table__cell">2. ГазИнвестТруб</td>
                                                    <td class="date table__cell">11 дек. 2018</td>
                                                    <td class="create table__cell">Самышкина Т.В.</td>
                                                    <td class="sum table__cell">2 000 000</td>
                                                </tr>
                                                <tr class="table__row lock">
                                                    <td class="name table__cell">3. Мостекстиль</td>
                                                    <td class="date table__cell">12 дек. 2018</td>
                                                    <td class="create table__cell">Гераськин И.В.</td>
                                                    <td class="sum table__cell">238 000</td>
                                                </tr>
                                                <tr class="table__row lock">
                                                    <td class="name table__cell">4. Охрана</td>
                                                    <td class="date table__cell">15 дек. 2018</td>
                                                    <td class="create table__cell">Егоров Д.В.</td>
                                                    <td class="sum table__cell">150 000</td>
                                                </tr>
                                                <tr class="table__row lock">
                                                    <td class="name table__cell">5. Едимкатим</td>
                                                    <td class="date table__cell">19 дек. 2018</td>
                                                    <td class="create table__cell">Павликовская Е.Н.</td>
                                                    <td class="sum table__cell">15 547</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center">
                                <a class="button mini white">Все оплаченные</a>
                            </div>
                        </div>
                    </div>
                    <div class="slideup__item open">
                        <div class="slideup__header d-flex justify-content-between">
                            <div class="slideup__heading d-flex">
                                <div class="slideup__btn">
                                    <a><span></span></a>
                                </div>
                                <div class="slideup__heading__text">
                                    Отказано в лизинге
                                </div>
                                <div class="count__caption">
                                    3
                                </div>
                            </div>
                            <div class="slideup__actions d-flex">
                                <select class="select2">
                                    <option>Январь</option>
                                    <option>Февраль</option>
                                    <option>Март</option>
                                    <option>Апрель</option>
                                    <option>Май</option>
                                    <option>Июнь</option>
                                    <option>Июль</option>
                                    <option>Август</option>
                                    <option>Сентябрь</option>
                                    <option>Октябрь</option>
                                    <option>Ноябрь</option>
                                    <option selected>Декабрь</option>
                                </select>
                                <select class="select2">
                                    <option>2015</option>
                                    <option>2016</option>
                                    <option>2017</option>
                                    <option selected>2018</option>
                                    <option>2019</option>
                                </select>
                                <div class="slideup__price">
                                    120 000 000 р.
                                </div>
                            </div>
                        </div>
                        <div class="slideup__body">
                            <div class="slideup__body__inner row">
                                <div class="col-12">
                                    <div class="table__wrapper">
                                        <table id="customtable3" class="customtable table">
                                            <thead class="table__row table__head">
                                            <tr>
                                                <td class="sort table__cell" data-sort="name">Клиент</td>
                                                <td class="sort table__cell" data-sort="date">Дата оплаты</td>
                                                <td class="sort table__cell" data-sort="create">Кем создано</td>
                                                <td class="sort table__cell" data-sort="sum">Сумма, р.</td>
                                            </tr>
                                            </thead>
                                            <tbody class="list">
                                            <tr class="table__row lock">
                                                <td class="name table__cell">1. Предприниматель</td>
                                                <td class="date table__cell">9 дек. 2018</td>
                                                <td class="create table__cell">Денисов Н.Б.</td>
                                                <td class="sum table__cell">500 000</td>
                                            </tr>
                                            <tr class="table__row lock">
                                                <td class="name table__cell">2. ГазИнвестТруб</td>
                                                <td class="date table__cell">11 дек. 2018</td>
                                                <td class="create table__cell">Самышкина Т.В.</td>
                                                <td class="sum table__cell">2 000 000</td>
                                            </tr>
                                            <tr class="table__row lock">
                                                <td class="name table__cell">3. Мостекстиль</td>
                                                <td class="date table__cell">12 дек. 2018</td>
                                                <td class="create table__cell">Гераськин И.В.</td>
                                                <td class="sum table__cell">238 000</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center">
                                <a class="button mini white">Все отказы</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?include "footer.php";?>
