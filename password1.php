<?include "header.php";?>
<section class="wrapper enterform__block">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <form class="enterform col-md-6 col-12" action="password2.php">
                <div class="enterform__logo col-4"><img src="images/logo.svg"/></div>
                <div class="enterform__heading">Восстановление пароля</div>
                <div class="enterform__text col-10 d-flex align-items-center">
                    <div class="image">
                        <img src="images/img-enter.png"/>
                    </div>
                    <div class="text">
                        Итак, для восстановления Вам нужно ввести свою почту, а мы пришлем ссылку для смены пароля.
                    </div>
                </div>
                <div class="enterform__form col-md-6 col-12">
                    <div class="formfield mailfield">
                        <input required placeholder="Email" type="text"/>
                        <div class="formfield-control"></div>
                    </div>
                    <div class="formfield buttonfield">
                        <button class="button blue">Отправить</button>
                    </div>
                </div>
            </form>
            <div class="techsupp__block">
                <a href="#">Техническая поддержка</a>
            </div>
        </div>
    </div>
</section>
<?include "footer.php";?>
