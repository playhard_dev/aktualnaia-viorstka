<?include "header.php";?>
<?include "include/public_header.php";?>
<section class="wrapper error__block">
    <div class="container">
        <div class="error__code">
            <span>404</span>
        </div>
        <div class="error__text">
            Что-то пошло не так, но мы уже сообщили нашему программисту.
            Он скоро все починит.
        </div>
        <div class="error__btn">
            <a href="/" class="button blue">На главную страницу</a>
        </div>
    </div>
</section>
<?include "footer.php";?>
