<?include "header.php";?>
<section class="wrapper enterform__block">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <form class="enterform col-md-6 col-12" action="index.php">
                <div class="enterform__logo col-4"><img src="images/logo.svg"/></div>
                <div class="enterform__heading">Вход в систему</div>
                <div class="enterform__form col-md-6 col-12">
                    <div class="formfield mailfield">
                        <input required placeholder="Логин или e-mail" type="text"/>
                    </div>
                    <div class="formfield passwordfield">
                        <input required placeholder="Пароль" type="password"/>
                        <div class="formfield-control"></div>
                    </div>
                    <div class="formfield linkfield">
                        <a href="password1.php">Забыли пароль?</a>
                    </div>
                    <div class="formfield buttonfield">
                        <button class="button blue">Продолжить</button>
                    </div>
                    <div class="formfield formfooter">
                        <img src="images/login-img.png"/>
                    </div>
                </div>
            </form>
            <div class="techsupp__block">
                <a href="#">Техническая поддержка</a>
            </div>
        </div>
    </div>
</section>
<?include "footer.php";?>
