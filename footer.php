        <footer class="wrapper footer__block"></footer>
        <section id="popup1" class="wrapper popup__block">
            <div class="container d-flex">
                <div class="popup__bglay"></div>
                <div class="popup__wrapper">
                    <span class="popup__close">
                        <span class="icon-icons-08"></span>
                    </span>
                    <div class="popup__heading">
                        Предыдущие сделки
                    </div>
                    <div class="table__wrapper">
                        <div class=" scrollblock" data-axis="y">
                        <table id="customtable2" class="customtable table">
                            <thead class="table__row table__head">
                                <tr>
                                    <td width="20%" class="sort table__cell" data-sort="date">Дата</td>
                                    <td width="20%" class="sort table__cell" data-sort="bill">ПЛ</td>
                                    <td width="20%" class="sort table__cell" data-sort="status">Статус</td>
                                    <td width="20%" class="sort table__cell" data-sort="source">Площадка</td>
                                    <td width="20%" class="sort table__cell" data-sort="manager">Контактное лицо</td>
                                </tr>
                            </thead>
                            <tbody class="list">
                                <tr class="table__row">
                                    <td class="date table__cell">14 мар. 2018</td>
                                    <td class="bill table__cell">ПЛ №104567</td>
                                    <td class="status table__cell">КП</td>
                                    <td class="source table__cell">Северсталь</td>
                                    <td class="manager table__cell">Денисов Н.Б.</td>
                                </tr>
                                <tr class="table__row">
                                    <td class="date table__cell">14 мар. 2018</td>
                                    <td class="bill table__cell">ПЛ №104567</td>
                                    <td class="status table__cell">КП</td>
                                    <td class="source table__cell">Северсталь</td>
                                    <td class="manager table__cell">Денисов Н.Б.</td>
                                </tr>
                                <tr class="table__row">
                                    <td class="date table__cell">14 мар. 2018</td>
                                    <td class="bill table__cell">ПЛ №104567</td>
                                    <td class="status table__cell">КП</td>
                                    <td class="source table__cell">Северсталь</td>
                                    <td class="manager table__cell">Денисов Н.Б.</td>
                                </tr>
                                <tr class="table__row">
                                    <td class="date table__cell">14 мар. 2018</td>
                                    <td class="bill table__cell">ПЛ №104567</td>
                                    <td class="status table__cell">КП</td>
                                    <td class="source table__cell">Северсталь</td>
                                    <td class="manager table__cell">Денисов Н.Б.</td>
                                </tr>
                                <tr class="table__row">
                                    <td class="date table__cell">14 мар. 2018</td>
                                    <td class="bill table__cell">ПЛ №104567</td>
                                    <td class="status table__cell">КП</td>
                                    <td class="source table__cell">Северсталь</td>
                                    <td class="manager table__cell">Денисов Н.Б.</td>
                                </tr>
                                <tr class="table__row">
                                    <td class="date table__cell">14 мар. 2018</td>
                                    <td class="bill table__cell">ПЛ №104567</td>
                                    <td class="status table__cell">КП</td>
                                    <td class="source table__cell">Северсталь</td>
                                    <td class="manager table__cell">Денисов Н.Б.</td>
                                </tr>
                                <tr class="table__row">
                                    <td class="date table__cell">14 мар. 2018</td>
                                    <td class="bill table__cell">ПЛ №104567</td>
                                    <td class="status table__cell">КП</td>
                                    <td class="source table__cell">Северсталь</td>
                                    <td class="manager table__cell">Денисов Н.Б.</td>
                                </tr>
                                <tr class="table__row">
                                    <td class="date table__cell">14 мар. 2018</td>
                                    <td class="bill table__cell">ПЛ №104567</td>
                                    <td class="status table__cell">КП</td>
                                    <td class="source table__cell">Северсталь</td>
                                    <td class="manager table__cell">Денисов Н.Б.</td>
                                </tr>
                                <tr class="table__row">
                                    <td class="date table__cell">14 мар. 2018</td>
                                    <td class="bill table__cell">ПЛ №104567</td>
                                    <td class="status table__cell">КП</td>
                                    <td class="source table__cell">Северсталь</td>
                                    <td class="manager table__cell">Денисов Н.Б.</td>
                                </tr>
                                <tr class="table__row">
                                    <td class="date table__cell">14 мар. 2018</td>
                                    <td class="bill table__cell">ПЛ №104567</td>
                                    <td class="status table__cell">КП</td>
                                    <td class="source table__cell">Северсталь</td>
                                    <td class="manager table__cell">Денисов Н.Б.</td>
                                </tr>
                                <tr class="table__row">
                                    <td class="date table__cell">14 мар. 2018</td>
                                    <td class="bill table__cell">ПЛ №104567</td>
                                    <td class="status table__cell">КП</td>
                                    <td class="source table__cell">Северсталь</td>
                                    <td class="manager table__cell">Денисов Н.Б.</td>
                                </tr>
                                <tr class="table__row">
                                    <td class="date table__cell">14 мар. 2018</td>
                                    <td class="bill table__cell">ПЛ №104567</td>
                                    <td class="status table__cell">КП</td>
                                    <td class="source table__cell">Северсталь</td>
                                    <td class="manager table__cell">Денисов Н.Б.</td>
                                </tr>
                                <tr class="table__row">
                                    <td class="date table__cell">14 мар. 2018</td>
                                    <td class="bill table__cell">ПЛ №104567</td>
                                    <td class="status table__cell">КП</td>
                                    <td class="source table__cell">Северсталь</td>
                                    <td class="manager table__cell">Денисов Н.Б.</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="popup2" class="wrapper popup__block">
            <div class="container d-flex">
                <div class="popup__bglay"></div>
                <div class="popup__wrapper mini">
                    <span class="popup__close">
                        <span class="icon-icons-08"></span>
                    </span>
                    <div class="popup__heading center">
                        <img src="images/illus-02.svg"/>
                        Поздравляем! Расчет сформирован
                    </div>
                    <div class="popup__action__btn">
                        <a class="button blue" data-action="close">Готово</a>
                    </div>
                </div>
            </div>
        </section>
        <section id="popup3" class="wrapper popup__block">
            <div class="container d-flex">
                <div class="popup__bglay"></div>
                <div class="popup__wrapper middle">
                    <span class="popup__close">
                        <span class="icon-icons-08"></span>
                    </span>
                    <div class="popup__heading">
                        График платежей
                    </div>
                    <div class="tabs__container">
                        <div class="tabs">
                            <div class="tabs__item active">
                                <div class="value">Платеж с дегрессией</div>
                            </div>
                            <div class="tabs__item">
                                <div class="value">Плавно убывающий</div>
                            </div>
                        </div>
                        <div class="tabs__contents">
                            <div class="tabs__content active">
                                <div class="table__wrapper">
                                    <div class="scrollblock" data-axis="y">
                                        <table id="customtable4" class="customtable table">
                                            <thead class="table__row table__head">
                                            <tr>
                                                <td class="sort table__cell" data-sort="period">Лизинговый период</td>
                                                <td class="sort table__cell" data-sort="type">Вид платежа</td>
                                                <td class="sort table__cell" data-sort="sum">Сумма к оплате с НДС, р.</td>
                                            </tr>
                                            </thead>
                                            <tbody class="list">
                                            <tr class="table__row">
                                                <td class="period table__cell center">1</td>
                                                <td class="type table__cell">Аванс</td>
                                                <td class="sum table__cell">600 000</td>
                                            </tr>
                                            <?for($i = 2; $i < 25; $i++):?>
                                                <tr class="table__row">
                                                    <td class="period table__cell center"><?=$i?></td>
                                                    <td class="type table__cell">Лизинговый платеж</td>
                                                    <td class="sum table__cell">110 713</td>
                                                </tr>
                                            <?endfor;?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table__footer d-flex justify-content-between align-items-center">
                                        <a class="button blue"><span class="icon-icons-26"></span>Скачать pdf</a>
                                        <div class="table__total__price">
                                            ИТОГО: 28 059 016 р.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__content">
                                <div class="table__wrapper">
                                    <div class="scrollblock" data-axis="y">
                                        <table id="customtable5" class="customtable table">
                                            <thead class="table__row table__head">
                                                <tr>
                                                    <td class="sort table__cell" data-sort="period">Лизинговый период</td>
                                                    <td class="sort table__cell" data-sort="type">Вид платежа</td>
                                                    <td class="sort table__cell" data-sort="sum">Сумма к оплате с НДС, р.</td>
                                                </tr>
                                            </thead>
                                            <tbody class="list">
                                            <tr class="table__row">
                                                <td class="period table__cell center">1</td>
                                                <td class="type table__cell">Аванс</td>
                                                <td class="sum table__cell">600 000</td>
                                            </tr>
                                            <?for($i = 2; $i < 25; $i++):?>
                                                <tr class="table__row">
                                                    <td class="period table__cell center"><?=$i?></td>
                                                    <td class="type table__cell">Лизинговый платеж</td>
                                                    <td class="sum table__cell">111 111</td>
                                                </tr>
                                            <?endfor;?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table__footer d-flex justify-content-between align-items-center">
                                        <a class="button blue"><span class="icon-icons-26"></span>Скачать pdf</a>
                                        <div class="table__total__price">
                                            ИТОГО: 28 059 016 р.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <section id="popup4" class="wrapper popup__block">
            <div class="container d-flex">
                <div class="popup__bglay"></div>
                <div class="popup__wrapper mini">
                    <span class="popup__close">
                        <span class="icon-icons-08"></span>
                    </span>
                    <div class="popup__heading center">
                        <img src="images/illus-05.svg"/>
                        <div>
                        Мы теряем контроль!<br/>
                        Ошибка в расчете
                        </div>
                    </div>
                    <div class="popup__action__btn">
                        <a class="button blue" data-action="close">Готово</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="wrapper mobilemenu">
            <div class="mobilemenu__bg"></div>
            <div class="mobilemenu__content">
                <a  href="user.php" class="header__userinfo col-12 d-flex align-items-center justify-content-between">
                    <div class="textinfo">
                        <div class="name">Раскольников Ф.М.</div>
                        <div class="role">Руководитель</div>
                    </div>
                    <div class="photo" style="background-image: url(images/man_mini.png);"></div>
                </a>
                <div class="mainsidebar col-12 p-0">
                    <div class="mainmenu">
                        <a class="d-flex">
                            <span class="icon-icons-21"></span>
                            <span class="label">Финансовые показатели</span>
                        </a>
                        <a class="d-flex">
                            <span class="icon-icons-19"></span>
                            <span class="label">Создать расчет</span>
                        </a>
                        <a class="d-flex">
                            <span class="icon-icons-18"></span>
                            <span class="label">Отправить на расчет</span>
                        </a>
                        <a class="d-flex">
                            <span class="icon-icons-20"></span>
                            <span class="label">Сводные данные</span>
                        </a>
                        <a class="d-flex">
                            <span class="icon-icons-25"></span>
                            <span class="label">Сотрудники</span>
                        </a>
                    </div>
                    <div class="mainsidebar__contacts col-12 d-flex flex-wrap">
                        <div>
                            <span class="icon-icons-17"></span>
                        </div>
                        <div>
                            <div class="label">Техническая поддержка</div>
                            <div class="value"><a href="tel:8 (800) 656-65-65">8 (800) 656-65-65</a></div>
                        </div>
                    </div>
                    <div class="mainsidebar__exit col-12 d-flex flex-wrap">
                        <div>
                            <span class="icon-icons-28"></span>
                        </div>
                        <div>
                            <div class="value"><a href="login.php">Выйти</a></div>
                        </div>
                    </div>
                    <div class="manager col-12">
                        <div class="manager__heading">Ваш менеджер:</div>
                        <div class="manager__photo" style="background-image: url(images/man_mini.png);"></div>
                        <div class="manager__name">Голованов К.Н.</div>
                        <div class="manager__role">
                            старший менеджер лизингового отдела
                        </div>
                        <div class="manager__phones">
                            <a>+7 (925) 456-63-90</a>
                            <a>+7 (925) 434-63-32</a>
                        </div>
                        <div class="manager__mail">
                            <a>fedor3000@element-lizing.ru</a>
                        </div>
                        <div class="manager__telegram">
                            <a>@element_fedor3000</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>