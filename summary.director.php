<?include "header.php";?>
<?include "include/public_header.php";?>
    <section class="wrapper main__screen">
        <div class="container">
            <div class="row">
                <?include "include/sidebar.php";?>
                <div class="maincontainer col-10">
                    <div class="heading__block d-flex justify-content-between align-items-center">
                        <div class="heading__text">
                            Сводные данные
                        </div>
                        <div class="heading__actions">
                            <div class="button__group">
                                <a class="button micro blue">Cегодня</a>
                                <a class="button micro blue">5 дней</a>
                                <a class="button micro blue active">1 месяц</a>
                                <span class="button__group__label">Период</span>
                            </div>
                            <!--<select class="select2">
                                <option>Дополнительно</option>
                            </select>-->
                        </div>
                    </div>
                    <div class="block__heading">Объекты лизинга на рассчете</div>
                    <div class="tabs">
                        <div class="tabs__item active">
                            <div class="count__caption">253</div>
                            <div class="value">Все</div>
                        </div>
                        <div class="tabs__item">
                            <div class="count__caption">15</div>
                            <div class="value">Активные</div>
                        </div>
                        <div class="tabs__item">
                            <div class="count__caption">6</div>
                            <div class="value">Оплаченные</div>
                        </div>
                        <div class="tabs__item">
                            <div class="count__caption">2</div>
                            <div class="value">Отказ</div>
                        </div>
                        <div class="tabs__item">
                            <div class="count__caption">10</div>
                            <div class="value">Акт ПП</div>
                        </div>
                    </div>
                    <div class="table__wrapper">
                        <table id="customtable" class="customtable table">
                            <thead class="table__row table__head">
                                <tr>
                                    <td class="sort table__cell" data-sort="name">Клиент</td>
                                    <td class="sort table__cell" data-sort="status">Статус</td>
                                    <td class="sort table__cell" data-sort="model">Марка / Модель</td>
                                    <td class="sort table__cell" data-sort="date">Дата создания</td>
                                    <td class="sort table__cell" data-sort="sum">Сумма, р.</td>
                                </tr>
                            </thead>
                            <tbody class="list">
                                <tr class="table__row lock">
                                    <td class="name table__cell">Предприниматель</td>
                                    <td class="status table__cell">
                                        <div class="status__block">
                                            <div class="status__group">
                                                <div class="status__item green">
                                                    <span>
                                                        Проект утвержден, договору присвоен номер <b>РАС-565998</b>
                                                    </span>
                                                </div>
                                                <div class="status__item green">
                                                    <span>
                                                        Проект утвержден, договору присвоен номер <b>РАС-565998</b>
                                                    </span>
                                                </div>
                                                <div class="status__item green">
                                                    <span>
                                                        Проект утвержден, договору присвоен номер <b>РАС-565998</b>
                                                    </span>
                                                </div>
                                                <div class="status__item green">
                                                    <span>
                                                        Проект утвержден, договору присвоен номер <b>РАС-565998</b>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="status__group">
                                                <div class="status__item green">
                                                    <span>
                                                        Проект утвержден, договору присвоен номер <b>РАС-565998</b>
                                                    </span>
                                                </div>
                                                <div class="status__item red">
                                                    <span>
                                                        Проект не утвержден
                                                    </span>
                                                </div>
                                                <div class="status__item"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="model table__cell">Doosan Model 2486</td>
                                    <td class="date table__cell">9 дек. 2018</td>
                                    <td class="sum table__cell">500 000</td>
                                </tr>
                                <tr class="table__row lock">
                                    <td class="name table__cell">ГазИнвестТруб</td>
                                    <td class="status table__cell">
                                        <div class="status__block">
                                            <div class="status__group">
                                                <div class="status__item green"></div>
                                                <div class="status__item green"></div>
                                                <div class="status__item red"></div>
                                                <div class="status__item "></div>
                                            </div>
                                            <div class="status__group">
                                                <div class="status__item "></div>
                                                <div class="status__item "></div>
                                                <div class="status__item"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="model table__cell">ГАЗ 256</td>
                                    <td class="date table__cell">11 дек. 2018</td>
                                    <td class="sum table__cell">8 000 000</td>
                                </tr>
                                <tr class="table__row lock">
                                    <td class="name table__cell">Мостекстиль</td>
                                    <td class="status table__cell">
                                        <div class="status__block">
                                            <div class="status__group">
                                                <div class="status__item green"></div>
                                                <div class="status__item green"></div>
                                                <div class="status__item green"></div>
                                                <div class="status__item green"></div>
                                            </div>
                                            <div class="status__group">
                                                <div class="status__item green"></div>
                                                <div class="status__item "></div>
                                                <div class="status__item"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="model table__cell">КамАЗ 28789</td>
                                    <td class="date table__cell">12 дек. 2018</td>
                                    <td class="sum table__cell">20 000</td>
                                </tr>
                                <tr class="table__row lock">
                                    <td class="name table__cell">Охрана</td>
                                    <td class="status table__cell">
                                        <div class="status__block">
                                            <div class="status__group">
                                                <div class="status__item "></div>
                                                <div class="status__item "></div>
                                                <div class="status__item "></div>
                                                <div class="status__item "></div>
                                            </div>
                                            <div class="status__group">
                                                <div class="status__item "></div>
                                                <div class="status__item "></div>
                                                <div class="status__item"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="model table__cell">Mercedes Benz C</td>
                                    <td class="date table__cell">15 дек. 2018</td>
                                    <td class="sum table__cell">150 000</td>
                                </tr>
                                <tr class="table__row lock">
                                    <td class="name table__cell">Едимкатим</td>
                                    <td class="status table__cell">
                                        <div class="status__block">
                                            <div class="status__group">
                                                <div class="status__item "></div>
                                                <div class="status__item "></div>
                                                <div class="status__item "></div>
                                                <div class="status__item "></div>
                                            </div>
                                            <div class="status__group">
                                                <div class="status__item "></div>
                                                <div class="status__item "></div>
                                                <div class="status__item"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="model table__cell">Lada Kalina</td>
                                    <td class="date table__cell">16 дек. 2018</td>
                                    <td class="sum table__cell">15 547</td>
                                </tr>
                                <tr class="table__row lock">
                                    <td class="name table__cell">Предприниматель</td>
                                    <td class="status table__cell">
                                        <div class="status__block">
                                            <div class="status__group">
                                                <div class="status__item green"></div>
                                                <div class="status__item green"></div>
                                                <div class="status__item green"></div>
                                                <div class="status__item green"></div>
                                            </div>
                                            <div class="status__group">
                                                <div class="status__item green"></div>
                                                <div class="status__item red"></div>
                                                <div class="status__item"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="model table__cell">Doosan Model 2486</td>
                                    <td class="date table__cell">9 дек. 2018</td>
                                    <td class="sum table__cell">500 000</td>
                                </tr>
                                <tr class="table__row lock">
                                    <td class="name table__cell">ГазИнвестТруб</td>
                                    <td class="status table__cell">
                                        <div class="status__block">
                                            <div class="status__group">
                                                <div class="status__item green"></div>
                                                <div class="status__item green"></div>
                                                <div class="status__item red"></div>
                                                <div class="status__item "></div>
                                            </div>
                                            <div class="status__group">
                                                <div class="status__item "></div>
                                                <div class="status__item "></div>
                                                <div class="status__item"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="model table__cell">ГАЗ 256</td>
                                    <td class="date table__cell">11 дек. 2018</td>
                                    <td class="sum table__cell">8 000 000</td>
                                </tr>
                                <tr class="table__row lock">
                                    <td class="name table__cell">Мостекстиль</td>
                                    <td class="status table__cell">
                                        <div class="status__block">
                                            <div class="status__group">
                                                <div class="status__item green"></div>
                                                <div class="status__item green"></div>
                                                <div class="status__item green"></div>
                                                <div class="status__item green"></div>
                                            </div>
                                            <div class="status__group">
                                                <div class="status__item green"></div>
                                                <div class="status__item "></div>
                                                <div class="status__item"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="model table__cell">КамАЗ 28789</td>
                                    <td class="date table__cell">12 дек. 2018</td>
                                    <td class="sum table__cell">20 000</td>
                                </tr>
                                <tr class="table__row lock">
                                    <td class="name table__cell">Охрана</td>
                                    <td class="status table__cell">
                                        <div class="status__block">
                                            <div class="status__group">
                                                <div class="status__item "></div>
                                                <div class="status__item "></div>
                                                <div class="status__item "></div>
                                                <div class="status__item "></div>
                                            </div>
                                            <div class="status__group">
                                                <div class="status__item "></div>
                                                <div class="status__item "></div>
                                                <div class="status__item"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="model table__cell">Mercedes Benz C</td>
                                    <td class="date table__cell">15 дек. 2018</td>
                                    <td class="sum table__cell">150 000</td>
                                </tr>
                                <tr class="table__row lock">
                                    <td class="name table__cell">Едимкатим</td>
                                    <td class="status table__cell">
                                        <div class="status__block">
                                            <div class="status__group">
                                                <div class="status__item "></div>
                                                <div class="status__item "></div>
                                                <div class="status__item "></div>
                                                <div class="status__item "></div>
                                            </div>
                                            <div class="status__group">
                                                <div class="status__item "></div>
                                                <div class="status__item "></div>
                                                <div class="status__item"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="model table__cell">Lada Kalina</td>
                                    <td class="date table__cell">16 дек. 2018</td>
                                    <td class="sum table__cell">15 547</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="table__footer d-flex justify-content-end">
                            <div class="table__total__price">
                                ИТОГО: 28 059 016 р.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?include "footer.php";?>