<?include "header.php";?>
<?include "include/public_header.php";?>
<section id="popup-detail" class="wrapper popup__block request__detail__wrapper active">
    <div class="container d-flex">
        <div class="popup__bglay"></div>
        <div class="popup__wrapper">
            <span class="popup__close">
                <span class="icon-icons-08"></span>
            </span>
            <div class="popup__heading">
                Сделка «ГазИнвестТруб»
            </div>
            <div class="request__steps d-flex">
                <div class="request__step accept">
                    <div class="status">
                        <span></span>
                    </div>
                    <div class="name">Создана</div>
                </div>
                <div class="request__step accept">
                    <div class="status">
                        <span></span>
                    </div>
                    <div class="name">Согласована
                        с клиентом</div>
                </div>
                <div class="request__step current">
                    <div class="status">
                        <span></span>
                    </div>
                    <div class="name">Сбор
                        документов</div>
                </div>
                <div class="request__step">
                    <div class="status">
                        <span></span>
                    </div>
                    <div class="name">Подготовка
                        к подписанию</div>
                </div>
                <div class="request__step">
                    <div class="status">
                        <span></span>
                    </div>
                    <div class="name">Ожидание
                        аванса</div>
                </div>
                <div class="request__step">
                    <div class="status">
                        <span></span>
                    </div>
                    <div class="name">Подготовка
                        к передаче</div>
                </div>
            </div>
            <div class="request__detail">
                <div class="request__detail__items">
                    <div class="request__detail__item row">
                        <div class="label col-12 col-md-3">Площадка</div>
                        <div class="value col-12 col-md-9">
                            Луидор. Байкальская <a href="#" class="detail">подробнее</a>
                        </div>
                    </div>
                    <div class="request__detail__item row">
                        <div class="label col-12 col-md-3">Клиент / ИНН</div>
                        <div class="value col-12 col-md-9">
                            ООО «Росгазинвестфонд», 546795456232157
                        </div>
                    </div>
                    <div class="request__detail__item row">
                        <div class="label col-12 col-md-3">Модель / Характеристика</div>
                        <div class="value col-12 col-md-9">
                            JCB 360, строительная спецтехника
                        </div>
                    </div>
                    <div class="request__detail__item row">
                        <div class="label col-12 col-md-3">Количество ед.</div>
                        <div class="value col-12 col-md-9">
                            15
                        </div>
                    </div>
                    <div class="request__detail__item row">
                        <div class="label col-12 col-md-3">Расчет, №</div>
                        <div class="value col-12 col-md-9">
                            РАС-564687 <a href="#" class="detail">подробнее</a>
                        </div>
                    </div>
                    <div class="request__detail__item row">
                        <div class="label col-12 col-md-3">Дата создания расчета</div>
                        <div class="value col-12 col-md-9">
                            9 января 2018
                        </div>
                    </div>
                    <div class="request__detail__item row">
                        <div class="label col-12 col-md-3">Договор №</div>
                        <div class="value col-12 col-md-9">
                            АХ ЭЛ/МСК-312351/ДЛ <a href="#" class="detail">подробнее</a>
                            <div>
                                <span class="status">
                                    корректируется
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="request__detail__item row">
                        <div class="label col-12 col-md-3">Договор №</div>
                        <div class="value col-12 col-md-9">
                            —
                        </div>
                    </div>
                    <div class="request__detail__item row">
                        <div class="label col-12 col-md-3">Менеджер по лизингу</div>
                        <div class="value col-12 col-md-9">
                            Егорова Д.В.
                            <div class="reference">
                                <span>
                                    <a>8 (800) 453-23-31</a>
                                    <a>+7 (925)456-63-90</a>
                                    <a>kozin.info@yandex.ru</a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="request__detail__item row">
                        <div class="label col-12 col-md-3">Менеджер площадки</div>
                        <div class="value col-12 col-md-9">
                            Самышкин В.С.
                            <div class="reference">
                                <span>
                                    <a>8 (800) 453-23-31</a>
                                    <a>+7 (925)456-63-90</a>
                                    <a>kozin.info@yandex.ru</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="request__footer">
                <div class="request__footer__heading">ИТОГО: 28 059 016 р.</div>
                <div class="request__footer__text"><img src="images/checked.svg"/><span>С НДС 10%</span></div>
            </div>
        </div>
    </div>
</section>
<section class="wrapper main__screen">
    <div class="container">
        <div class="row">
            <?include "include/sidebar.php";?>
            <div class="maincontainer col-lg-10 col-12">
                <div class="heading__block d-flex justify-content-between align-items-center">
                    <div class="heading__text">
                        Сводные данные
                    </div>
                    <div class="heading__actions">
                        <div class="button__group">
                            <a class="button micro blue">Cегодня</a>
                            <a class="button micro blue">5 дней</a>
                            <a class="button micro blue active">1 месяц</a>
                            <span class="button__group__label">Период</span>
                        </div>
                        <!--<select class="select2">
                            <option>Дополнительно</option>
                        </select>-->
                    </div>
                </div>
                <div class="block__heading">Объекты лизинга на рассчете</div>
                <div class="tabs">
                    <div class="tabs__item active">
                        <div class="count__caption">5</div>
                        <div class="value">Все</div>
                    </div>
                    <div class="tabs__item">
                        <div class="count__caption">7</div>
                        <div class="value">Активные</div>
                    </div>
                    <div class="tabs__item">
                        <div class="count__caption">0</div>
                        <div class="value">Оплаченные</div>
                    </div>
                    <div class="tabs__item">
                        <div class="count__caption">2</div>
                        <div class="value">Отказ</div>
                    </div>
                    <div class="tabs__item">
                        <div class="count__caption">0</div>
                        <div class="value">Акт ПП</div>
                    </div>
                </div>
                <div class="table__wrapper">
                    <table id="customtable" class="customtable table">
                        <thead class="table__row table__head">
                        <tr>
                            <td class="sort table__cell" data-sort="name">Клиент</td>
                            <td class="sort table__cell" data-sort="status">Статус</td>
                            <td class="sort table__cell" data-sort="model">Марка / Модель</td>
                            <td class="sort table__cell" data-sort="date">Дата создания</td>
                            <td class="sort table__cell" data-sort="sum">Сумма, р.</td>
                        </tr>
                        </thead>
                        <tbody class="list">
                        <tr class="table__row lock">
                            <td class="name table__cell">Предприниматель</td>
                            <td class="status table__cell">
                                <div class="status__block">
                                    <div class="status__group">
                                        <div class="status__item green">
                                                    <span>
                                                        Проект утвержден, договору присвоен номер <b>РАС-565998</b>
                                                    </span>
                                        </div>
                                        <div class="status__item green">
                                                    <span>
                                                        Проект утвержден, договору присвоен номер <b>РАС-565998</b>
                                                    </span>
                                        </div>
                                        <div class="status__item green">
                                                    <span>
                                                        Проект утвержден, договору присвоен номер <b>РАС-565998</b>
                                                    </span>
                                        </div>
                                        <div class="status__item green">
                                                    <span>
                                                        Проект утвержден, договору присвоен номер <b>РАС-565998</b>
                                                    </span>
                                        </div>
                                    </div>
                                    <div class="status__group">
                                        <div class="status__item green">
                                                    <span>
                                                        Проект утвержден, договору присвоен номер <b>РАС-565998</b>
                                                    </span>
                                        </div>
                                        <div class="status__item red">
                                                    <span>
                                                        Проект не утвержден
                                                    </span>
                                        </div>
                                        <div class="status__item"></div>
                                    </div>
                                </div>
                            </td>
                            <td class="model table__cell">Doosan Model 2486</td>
                            <td class="date table__cell">9 дек. 2018</td>
                            <td class="sum table__cell">500 000</td>
                        </tr>
                        <tr class="table__row lock">
                            <td class="name table__cell">ГазИнвестТруб</td>
                            <td class="status table__cell">
                                <div class="status__block">
                                    <div class="status__group">
                                        <div class="status__item green"></div>
                                        <div class="status__item green"></div>
                                        <div class="status__item red"></div>
                                        <div class="status__item "></div>
                                    </div>
                                    <div class="status__group">
                                        <div class="status__item "></div>
                                        <div class="status__item "></div>
                                        <div class="status__item"></div>
                                    </div>
                                </div>
                            </td>
                            <td class="model table__cell">ГАЗ 256</td>
                            <td class="date table__cell">11 дек. 2018</td>
                            <td class="sum table__cell">8 000 000</td>
                        </tr>
                        <tr class="table__row lock">
                            <td class="name table__cell">Мостекстиль</td>
                            <td class="status table__cell">
                                <div class="status__block">
                                    <div class="status__group">
                                        <div class="status__item green"></div>
                                        <div class="status__item green"></div>
                                        <div class="status__item green"></div>
                                        <div class="status__item green"></div>
                                    </div>
                                    <div class="status__group">
                                        <div class="status__item green"></div>
                                        <div class="status__item "></div>
                                        <div class="status__item"></div>
                                    </div>
                                </div>
                            </td>
                            <td class="model table__cell">КамАЗ 28789</td>
                            <td class="date table__cell">12 дек. 2018</td>
                            <td class="sum table__cell">20 000</td>
                        </tr>
                        <tr class="table__row lock">
                            <td class="name table__cell">Охрана</td>
                            <td class="status table__cell">
                                <div class="status__block">
                                    <div class="status__group">
                                        <div class="status__item "></div>
                                        <div class="status__item "></div>
                                        <div class="status__item "></div>
                                        <div class="status__item "></div>
                                    </div>
                                    <div class="status__group">
                                        <div class="status__item "></div>
                                        <div class="status__item "></div>
                                        <div class="status__item"></div>
                                    </div>
                                </div>
                            </td>
                            <td class="model table__cell">Mercedes Benz C</td>
                            <td class="date table__cell">15 дек. 2018</td>
                            <td class="sum table__cell">150 000</td>
                        </tr>
                        <tr class="table__row lock">
                            <td class="name table__cell">Едимкатим</td>
                            <td class="status table__cell">
                                <div class="status__block">
                                    <div class="status__group">
                                        <div class="status__item "></div>
                                        <div class="status__item "></div>
                                        <div class="status__item "></div>
                                        <div class="status__item "></div>
                                    </div>
                                    <div class="status__group">
                                        <div class="status__item "></div>
                                        <div class="status__item "></div>
                                        <div class="status__item"></div>
                                    </div>
                                </div>
                            </td>
                            <td class="model table__cell">Lada Kalina</td>
                            <td class="date table__cell">16 дек. 2018</td>
                            <td class="sum table__cell">15 547</td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="table__footer d-flex justify-content-end">
                        <div class="table__total__price">
                            ИТОГО: 28 059 016 р.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?include "footer.php";?>